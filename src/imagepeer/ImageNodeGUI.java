/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imagepeer;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author zoewarena
 */
public class ImageNodeGUI extends JPanel implements ActionListener {
    //stores/initializes component variables for the GUI

    private JLabel welcomeMessage, welcomeMessage2, idLabel, idLabel2, displayImage, timeStamp;
    private JComboBox imagesMenu;
    private JTextField timeStampMessage;
    private JButton joinSystem, leaveSystem, getImage, getTimeStamp, saveImage;
    private ImageNodeImpl node;
    private ArrayList imagesMenuList;
    private String filename;
    private ImageIcon image;
    public ImageNodeGUI() {
        //initializes variables
        imagesMenuList = new ArrayList();
        //sets the layout and size for GUI
        setPreferredSize(new Dimension(300, 350));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        welcomeMessage = new JLabel("Welcome");
        welcomeMessage2 = new JLabel("Please join system to select your Image");

        JPanel idPanel = new JPanel();
        idLabel = new JLabel("ID:");
        idLabel2 = new JLabel();
        idPanel.add(idLabel);
        idPanel.add(idLabel2);

        JPanel buttons = new JPanel();
        joinSystem = new JButton("Join");
        leaveSystem = new JButton("Leave");
        joinSystem.addActionListener(this);
        leaveSystem.addActionListener(this);
        leaveSystem.setEnabled(false);
        buttons.add(joinSystem);
        buttons.add(leaveSystem);

        imagesMenu = new JComboBox();
        imagesMenu.addActionListener(this);

        JPanel buttons2 = new JPanel();
         getImage = new JButton("Get");
        saveImage = new JButton("Save");
         getImage.addActionListener(this);
        saveImage.addActionListener(this);
        buttons2.add(getImage);
        buttons2.add(saveImage);
        
        getTimeStamp = new JButton("Predicate");
        getTimeStamp.addActionListener(this);
        
        displayImage = new JLabel();
        
        timeStamp = new JLabel();
        
        

        add(welcomeMessage);
        add(welcomeMessage2);
        add(idPanel);
        add(buttons);
        add(imagesMenu);
        add(buttons2);
        add(timeStamp);
        add(displayImage);
        add(getTimeStamp);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == joinSystem) {
                //sets button enablements
                leaveSystem.setEnabled(true);
                joinSystem.setEnabled(false);
                
                //for dialogbox to enter directory path for where images will come from
                String directoryInput = "";
                File[] fileArray = null;
                String displayMessage = "";
                while(fileArray == null){
                directoryInput = (String) JOptionPane.showInputDialog(
                        this, displayMessage + 
                        "Enter image directory",
                        "Customized Dialog",
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        null,
                        "");
                File file = new File(directoryInput);
                fileArray = file.listFiles();
                displayMessage = "No such directory. ";
                }
                
                //creates new node/server which is registered, and gets peers in network 
                //and adds images from chosen directory to its imagelist
                node = new ImageNodeImpl();
                node.registerStub();
                node.populateClients();
                try {
                    node.populateImageList(directoryInput);
                } catch (Exception exe){
                    //changed into while loop for dialog box, so exception will be caught after first error
                }
                
                //starts threads/timerTasks which update the node/servers imagelist and peerslist
                getImageList thread = new getImageList();
                thread.start();
                Timer timer2 = new Timer();
                timer2.schedule(new getClientList(), 0, 5000);
                
                //displays nodeid to gui
                idLabel2.setText(node.nodeId);
                

        } else if (source == leaveSystem) {
            //sets enablements for buttons
            joinSystem.setEnabled(true);
            leaveSystem.setEnabled(false);
            
            //removes infor/images from display
            imagesMenu.removeAllItems();
            displayImage.setIcon(null);
            
             try {
                //removes itself from all its peers peer lists
                for(ImageNodeInterface peer: node.getClientArray(node.getVectorTimeStamp())){
                    peer.removeNode(node, node.getVectorTimeStamp());
                }
                //removes itself from registry
                 Registry registry = LocateRegistry.getRegistry("localhost");
                 registry.unbind(idLabel2.getText());
            } catch (RemoteException ex) {
                Logger.getLogger(ImageNodeGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(ImageNodeGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
             node = null;
        }else if (source == getImage) {
            //only does actions if there are items listed in the menu
            if (imagesMenu.getItemCount() > 0) {
                ImageNodeInterface[] peerArray;
                try {
                    //changes state of node
                    node.reciving = true;
                    //gets peers
                    peerArray = node.getClientArray(node.getVectorTimeStamp());
                    //gets selected filename
                    filename = (String) imagesMenu.getSelectedItem();
                    //starts leaderElection to select server with best bandwidth
                    LeaderList elect = new LeaderList(peerArray, filename);
                   ImageNodeInterface leaderNode = elect.electLeader();
                    //createImageIcon with image of requestedImage
                    ImageIcon imageStart = leaderNode.requestImage(filename, node.getVectorTimeStamp());
                    //
                    leaderNode.sent(node.getVectorTimeStamp());
                    node.reciving = false;
                    node.updateVector(leaderNode.getVectorTimeStamp());
                    Image imgfirst = imageStart.getImage();
                    Image img = imgfirst.getScaledInstance(150, 250, java.awt.Image.SCALE_SMOOTH);
                    image = new ImageIcon(img);
                    displayImage.setIcon(image);
                } catch (RemoteException ex) {

                    Logger.getLogger(ImageNodeGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                timeStamp.setText(node.timeStampAsString());
            }        
            
        } else if (source == getTimeStamp){
            String message = "";
            Map<String, HashMap<String, Integer>> predicate = node.TimeStamp(node);
            for(String peerId : predicate.keySet()){
                message += peerId + " vector: \n";
                HashMap<String, Integer> vector = predicate.get(peerId);
                for(String vecId : vector.keySet()){
                    message += vecId + ":" + vector.get(vecId) + " ";
                }
                message += "\n\n";
            }
            JOptionPane.showMessageDialog(this, message);
        } else if(source == saveImage){
            if(image!=null){
                node.saveImage(image, filename);
            }
        }
        
    }

    public static void main(String[] args) throws RemoteException {
        JFrame frame = new JFrame("ImagePeer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new ImageNodeGUI());
        frame.pack();
        // position the frame in the middle of the screen
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenDimension = tk.getScreenSize();
        Dimension frameDimension = frame.getSize();
        frame.setLocation((screenDimension.width - frameDimension.width) / 2, (screenDimension.height - frameDimension.height) / 2);
        frame.setVisible(true);
    }

    private JPanel getGUI() {
        return this;
    }

    class getClientList extends TimerTask {

        public void run() {
            if(node !=null){
                node.populateClients();
            }
        }
    }

    /**
     * Thread to update the drop down box
     */
    class getImageList extends Thread {

        public void run() {
            while (true) {
                imagesMenu.removeAllItems();
                imagesMenuList.clear();
                if (node != null) {
                    try {
                        ImageNodeInterface[] clients = node.getClientArray(node.vectorTimeStamp);
                        for (ImageNodeInterface client : clients) {
                             client.addNode(node, node.vectorTimeStamp);
                             ArrayList<String> imageNamesFromClient = client.getImages(node.vectorTimeStamp);
                             for (String imageName : imageNamesFromClient) {
                                 if (!imagesMenuList.contains(imageName)) {
                                     imagesMenuList.add(imageName);
                                 }
                             }
                          }
                        String[] imagesMenuArray = new String[imagesMenuList.size()];
                        imagesMenuList.toArray(imagesMenuArray);
                        imagesMenu.setModel(new DefaultComboBoxModel(imagesMenuArray));
                    } catch (RemoteException ex) {
                        Logger.getLogger(ImageNodeGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //getGUI().updateUI();
                }
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ImageNodeGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
           }
        }
}
