/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package imagepeer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author James Fairburn
 */
public class LeaderList {
    Node root;
    ImageNodeInterface leader;
    
    public LeaderList(ImageNodeInterface[] peerArray, String filename){
        //Create linked list of peers containing the desiered filename
        Node prevNode = null;
        for(ImageNodeInterface peer: peerArray){
            try {
                if(peer.containsImage(filename, peer.getVectorTimeStamp())){
                    Node n = new Node(peer);
                    if(root == null){
                        root = n;
                        prevNode = n;
                    } else {
                        prevNode.next = n;
                    }
                    prevNode = n; 
                }
            } catch (RemoteException ex) {
                Logger.getLogger(LeaderList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        //turn the linked list into a ring
        Node currentNode = root;
        while(currentNode.next!=null){
            currentNode = currentNode.next;
        }
        currentNode.next = root;   
    }
    
    public ImageNodeInterface electLeader(){
        root.startElection();
        return leader;
    }
    
    public class Node{
        ImageNodeInterface data;
        Node next;
        String leaderId;
        boolean participant;
        
        /**
         * How to handle a changRobertsMessage from the pervious node
         * @param messType
         * @param candidate
         * @param bandwidth
         * @param vector 
         */
        public void changRobertsMessage(String messType, ImageNodeInterface candidate , int bandwidth, HashMap<String,Integer> vector){
            try {
                if(messType.equals("election")){
                    data.updateVector(vector);
                    int candidateBandWidith = candidate.getBandwidth(data.getVectorTimeStamp());
                    int currentBandwidth = data.getBandwidth(data.getVectorTimeStamp());
                        if(bandwidth > data.getBandwidth(data.getVectorTimeStamp())){
                            participant = true;
                            next.changRobertsMessage("election", candidate, candidateBandWidith, data.getVectorTimeStamp());
                        } else if(candidate.getBandwidth(data.getVectorTimeStamp()) == data.getBandwidth(data.getVectorTimeStamp())){
                            //2 diffrent peers may have the same bandwidth, do a tie breaker
                            if(!candidate.getNodeId(data.getVectorTimeStamp()).equals(data.getNodeId(data.getVectorTimeStamp()))){
                                if(candidate.getNodeId(data.getVectorTimeStamp()).compareTo(data.getNodeId(data.getVectorTimeStamp())) > 1) 
                                /*candidate had a bigger id*/{
                                    next.changRobertsMessage("election", candidate, candidateBandWidith, data.getVectorTimeStamp());
                                } else /*own had a bigger id*/{
                                    next.changRobertsMessage("election", data, currentBandwidth, data.getVectorTimeStamp());
                                }
                            } else {
                                next.changRobertsMessage("leader", data, currentBandwidth, data.getVectorTimeStamp());
                            }
                        } else {
                            if (!participant){
                                startElection();
                            } else {
                                
                            }
                        }

                } else if(messType.equals("leader")) {
                    leader = candidate;
                    participant = false;
                } else {
                    
                }
            } catch (RemoteException ex) {
                    Logger.getLogger(LeaderList.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Should never get here
        }
        
       public void startElection(){
           participant = true;
            try {
                next.changRobertsMessage("election", data, data.getBandwidth(data.getVectorTimeStamp()), data.getVectorTimeStamp());
            } catch (RemoteException ex) {
                Logger.getLogger(LeaderList.class.getName()).log(Level.SEVERE, null, ex);
            }
       } 
        public Node(ImageNodeInterface current){
            this.data = current;
            participant = false;
        }

        public void setCurrent(ImageNodeInterface current) {
            this.data = current;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public ImageNodeInterface getCurrent() {
            return data;
        }

        public Node getNext() {
            return next;
        }
        
    }
}