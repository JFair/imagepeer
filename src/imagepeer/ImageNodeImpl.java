/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagepeer;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author James Fairburn
 */
public class ImageNodeImpl implements ImageNodeInterface {

    //Predicate
    boolean ownTaken = false;
    Map<String, HashMap<String, Integer>> predicate = new HashMap<>();
    HashMap<String, Boolean> recivedNeigbours = new HashMap<>();
    
    //global timestamp states
    boolean sending;
    boolean reciving;
    
    //for electing leader
    int bandwidth;
    
    //for vector timestamps
    HashMap<String, Integer> vectorTimeStamp;
    
    //list of images in selected directory
    String directory;
    ArrayList<String> images;
    
   //unique identifier, also used to register to server
    String nodeId;
    
    //map of peers
    Map<String, ImageNodeInterface> clients;

    public ImageNodeImpl() {
        //initialises empty list of images and maps of clients/timestamps
        images = new ArrayList<>();
        clients = new HashMap<>();
        vectorTimeStamp = new HashMap<>();

        //generate a bandwdith between 100 and 600 
        bandwidth = 100 + (int) (Math.random() * 500);
        
        //initialises all states as false
        reciving = false;
        sending = false;
        
        //initialises unset unique id
        nodeId = "NotSet";
        
        //initialises null image directory
        directory = null;

        //Set up the registry
        try {
            Registry registry = LocateRegistry.createRegistry(1099);
        } catch (RemoteException ex) {
            System.out.println("Already registerd");
        }
    }

    //for global snapshotting
    @Override
    public boolean isSending(HashMap<String,Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        return sending;
    }

    //for global snapshotting
    @Override
    public boolean isReciving(HashMap<String,Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        return reciving;
    }

    //for leader election
    @Override
    public int getBandwidth(HashMap<String,Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        return bandwidth;
    }
    
    //for vector timestamping
    public HashMap<String, Integer> getVectorTimeStamp() {
        return vectorTimeStamp;
    }
    
    //for selecting image directory
    public String getDirectory() {
        return directory;
    }
    
    //for retrieving machines list of images
    @Override
    public ArrayList<String> getImages(HashMap<String,Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        return images;
    }

    /**
     * Gets an image from the Images directory
     *
     * @param fileName - Filename of the requested image
     * @return the found image
     * @throws RemoteException
     */
    //method for client to request an image from server node (this node) with the specified filename
    @Override
    public ImageIcon requestImage(String fileName, HashMap<String,Integer> senderVector) throws RemoteException {
        sending = true;
        updateVector(senderVector);
        BufferedImage image;
        ImageIcon icon=null;
        try {
            image = ImageIO.read(new File(directory+ "/" + fileName));
            icon = new ImageIcon(image);
        } catch (IOException ex) {
            Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return icon;
    }

    @Override
    public String getNodeId(HashMap<String, Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        return nodeId;
    }

    //for LeaderElection, checks if the specified filename (requested by client) is in
    //this nodes image directory (by checking the list of image filenames)
    @Override
    public Boolean containsImage(String fileName, HashMap<String, Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        return images.contains(fileName);
    }

    //returns an array of clients this node is peered with
    @Override
    public ImageNodeInterface[] getClientArray(HashMap<String, Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        ImageNodeInterface[] toReturn = new ImageNodeInterface[clients.size()];
        int count = 0;
        for (ImageNodeInterface peer : clients.values()) {
            toReturn[count] = peer;
            count++;
        }
        return toReturn;
    }
    
    public void registerStub() {
        try {
            ImageNodeInterface stub = (ImageNodeInterface) UnicastRemoteObject.exportObject(this, 0);
            Registry registry = LocateRegistry.getRegistry("localhost");
            String[] names = Naming.list("localhost");
            int numOfBindings = names.length;

            //re-get the name list
            names = Naming.list("localhost");

            //sets unique nodeIds for node before registering
            if (nodeId.equals("NotSet")) {
                if (numOfBindings == 0) {
                    nodeId = "peer" + 0;
                } else {
                    int highest= 0;
                    for(String name : names){
                        int nodeNum = Integer.parseInt(name.substring(12));
                        if(nodeNum > highest){
                            highest = nodeNum;
                        }
                    }
                    nodeId = "peer" + (highest+1);
                }
            }
            //registers this node to registry with unique nodeID
            registry.bind(nodeId, stub);
            //adds timestamp for this node to vector
            vectorTimeStamp.put(nodeId, 0);
        } catch (RemoteException ex) {
            Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AlreadyBoundException ex) {
            System.out.println("Same port");
        }

    }

    public synchronized void populateClients() {
        try {
            //gets localhost registry
            Registry registry = LocateRegistry.getRegistry("localhost");
            //gets all names in localhost registry
            String[] bindings = Naming.list("localhost");

            //iterates through the names in localhost registry
            for (String name : bindings) {
                if (!name.contains(nodeId) && !clients.containsKey(name)) {
                    System.out.println("client name:" + name.substring(8));
                    ImageNodeInterface remoteClient = (ImageNodeInterface) registry.lookup(name.substring(8));
                    clients.put(name.substring(8), remoteClient);
                    vectorTimeStamp.put(name.substring(8), 0);
                }
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Gets a list of images file names
     *
     * @param directory - directory to search for images
     */
    void populateImageList(String directory) {
        images.clear();
        this.directory = directory;
        File dir = new File(directory);
        File[] fileList = dir.listFiles();
        for (File child : fileList) {
            //Get a files extention
            String extention = "";
            int dotIndex = child.getName().lastIndexOf('.');
            extention = child.getName().substring(dotIndex + 1);

            //adds only jpg, png and gif files to images list
            if (extention.equals("jpg") || extention.equals("png") || extention.equals("gif")) {
                images.add(child.getName());
            }
        }
    }
    
    private void incrementOwnVector(){
        vectorTimeStamp.put(nodeId, vectorTimeStamp.get(nodeId) +1 );
    }
    
    //for timestamping
    @Override
    public void updateVector(Map<String, Integer> recivedVector){
        System.out.println("UPDATING");
        incrementOwnVector();
        Collection<String> keys = recivedVector.keySet();
        for(String key: keys){
            int ownVal = vectorTimeStamp.get(key);
            int recivVal = recivedVector.get(key);
            if(ownVal >= recivVal){
                vectorTimeStamp.put(key, vectorTimeStamp.get(key));
            } else {
                vectorTimeStamp.put(key, recivedVector.get(key));
            }
        }
    }

    @Override
    public boolean removeNode(ImageNodeInterface toRemove, HashMap<String,Integer> senderVector) throws RemoteException {
        updateVector(senderVector);
        if(clients.containsKey(toRemove.getNodeId(vectorTimeStamp))){
           clients.remove(toRemove.getNodeId(vectorTimeStamp));
           vectorTimeStamp.remove(toRemove.getNodeId(vectorTimeStamp));
           System.out.println("Im out!");
           return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean addNode(ImageNodeInterface toAdd, HashMap<String,Integer> senderVector) throws RemoteException { 
        if(!clients.containsKey(toAdd.getNodeId(vectorTimeStamp))){
            clients.put(toAdd.getNodeId(vectorTimeStamp), toAdd);
            vectorTimeStamp.put(toAdd.getNodeId(vectorTimeStamp), 0);
            updateVector(senderVector);
            return true;
        } else {
            return false;
        }
    }

    public String timeStampAsString(){
        String toReturn = "TimeStamp: ";
        Collection<String> keys = vectorTimeStamp.keySet();
        for(String key: keys){
            toReturn += key + ":" +vectorTimeStamp.get(key) + "-";
        }
        
        return toReturn.substring(0, toReturn.length()-1) /*returns toRetutn without the last '-'*/;
    }

    public boolean saveImage(ImageIcon icon, String fileName) {
        try {
//            BufferedImage image = (BufferedImage) icon.getImage();
            Image img = icon.getImage();
            // Create a buffered image with transparency
            BufferedImage image = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

            // Draw the image on to the buffered image
            Graphics2D bGr = image.createGraphics();
            bGr.drawImage(img, 0, 0, null);
            bGr.dispose();

            int dotIndex = fileName.lastIndexOf('.');
            String extention = fileName.substring(dotIndex + 1);
            ImageIO.write(image, extention, new File(directory + "/" + fileName));
            images.add(fileName);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public void sent(Map<String, Integer> recivedVector) throws RemoteException {
        updateVector(recivedVector);
        sending = false;
    }
    
    @Override
    public Map<String, HashMap<String, Integer>> TimeStamp(ImageNodeImpl sender){
        if(!ownTaken){
            try {
                predicate.put(nodeId, getVectorTimeStamp());
                ownTaken = true;
                for(ImageNodeInterface neigbour : getClientArray(vectorTimeStamp)){
                    predicate.put(neigbour.getNodeId(vectorTimeStamp), neigbour.getVectorTimeStamp());
                    recivedNeigbours.put(neigbour.getNodeId(vectorTimeStamp), true);
                }
            } catch (RemoteException ex) {
                Logger.getLogger(ImageNodeImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if(ownTaken && allNeigboursTaken()){
            return predicate;
        }
        return predicate;
    }
    
    private boolean allNeigboursTaken(){
        boolean toReturn = true;
        for(boolean neighbour: recivedNeigbours.values()){
            if(!neighbour){
                toReturn = false;
            }
        }
        return toReturn;
    }

    @Override
    public ImageNodeImpl getState() throws RemoteException {
        return this;
    }
    
}