/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imagepeer;

import java.awt.Image;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 *
 * @author fch7873
 */
public interface ImageNodeInterface extends Remote{ 
    public boolean isSending(HashMap<String,Integer> senderVector) throws RemoteException;
    public boolean isReciving(HashMap<String,Integer> senderVector) throws RemoteException;
    public int getBandwidth(HashMap<String,Integer> senderVector)throws RemoteException;
    public ArrayList<String> getImages(HashMap<String,Integer> senderVector)throws RemoteException;
    public ImageIcon requestImage(String fileName, HashMap<String,Integer> senderVector)throws RemoteException;
    public String getNodeId(HashMap<String,Integer> senderVector) throws RemoteException;
    public Boolean containsImage(String fileName, HashMap<String,Integer> senderVector) throws RemoteException;
    public ImageNodeInterface[] getClientArray(HashMap<String,Integer> senderVector) throws RemoteException;
    public boolean removeNode(ImageNodeInterface toRemove, HashMap<String,Integer> senderVector) throws RemoteException;
    public boolean addNode(ImageNodeInterface toAdd, HashMap<String,Integer> senderVector) throws RemoteException;
    public HashMap<String, Integer> getVectorTimeStamp() throws RemoteException;
    public void updateVector(Map<String, Integer> recivedVector) throws RemoteException;
    public void sent(Map<String, Integer> recivedVector) throws RemoteException;
    public Map<String, HashMap<String, Integer>> TimeStamp(ImageNodeImpl sender) throws RemoteException;
    public ImageNodeImpl getState() throws RemoteException;
}